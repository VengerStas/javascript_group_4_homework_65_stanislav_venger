import React, {Component, Fragment} from 'react';
import axios from '../../axios-page';
import Navigation from "../Navigation/Navigation";
import {CATEGORIES} from "../../constants";

import './EditPages.css';

class EditPages extends Component {
    state = {
        category: '',
        title: '',
        content: '',
    };

    valueChanged = event => {
        const {name, value} = event.target;
        this.setState({[name]: value});
    };

    getOption = event => {
        const category = event.target.value;
        axios.get('pages/' + category + '.json').then(response  => {
            console.log(response);
            this.setState({title: response.data.title, content: response.data.content, category})
        })
    };

    editHandler = event => {
      event.preventDefault();

      const currentPage = {
          title: this.state.title,
          content: this.state.content
      };

      axios.put('pages/' + this.state.category + '.json', currentPage).then(() => {
          this.props.history.push('/pages/' + this.state.category);
      })
    };

    render() {
        return (
            <Fragment>
                <Navigation/>
                <div className="form-block">
                    <h4 className="form-block-title">Edit page</h4>
                    <form className="form">
                        <label className="form-label">Select page</label>
                        <select name="category" className="select" onChange={event =>this.getOption(event)}>
                            <option value="default" hidden>Select category...</option>
                            {Object.keys(CATEGORIES).map(name => (
                                <option key={name} value={name}>{CATEGORIES[name]}</option>
                            ))};
                        </select>
                        <label className="form-label">Title:</label>
                        <input type="text" name="title" value={this.state.title}
                               onChange={this.valueChanged} placeholder="Enter a title for page" className="title-input"
                        />
                        <label className="form-label">Content:</label>
                        <textarea name="content" id="" cols="30" rows="10"
                                  value={this.state.content} onChange={this.valueChanged} className="content-input"
                        />
                        <button type="submit" onClick={this.editHandler} className="btn">Save</button>
                    </form>
                </div>
            </Fragment>
        );
    }
}

export default EditPages;
