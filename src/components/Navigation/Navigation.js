import React from 'react';
import {NavLink} from "react-router-dom";

import './Navigation.css';

const Navigation = () => {
    return (
        <header className="header">
            <nav className="nav">
                <ul className="nav-menu">
                    <li className="nav-item">
                        <NavLink to="/pages/main" className="nav-link">Main</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink to="/pages/about" className="nav-link">About</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink to="/pages/clients" className="nav-link">Our Clients</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink to="/pages/cooperation" className="nav-link">Cooperation</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink to="/pages/contacts" className="nav-link">Contacts</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink to="/pages/admin" className="nav-link">Admin</NavLink>
                    </li>
                </ul>
            </nav>
        </header>
    );
};

export default Navigation;
