import React, {Component, Fragment} from 'react';
import axios from "../../axios-page";
import Navigation from "../Navigation/Navigation";

import './Pages.css';

class Pages extends Component {
    state = {
        title: '',
        content: ''
    };
    componentDidMount() {
        console.log(this.props.match.params.name);
        if (!this.props.match.params.name) return;
        axios.get('pages/' +  this.props.match.params.name + '.json').then(response => {
            this.setState({title: response.data.title, content: response.data.content});
        });
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.match.params.name !== prevProps.match.params.name) {
            axios.get('pages/' +  this.props.match.params.name + '.json').then(response => {
                this.setState({title: response.data.title, content: response.data.content});
            });
        }
    }

    render() {
        return (
            <Fragment>
                <Navigation/>
                <div className="content-block" />
                <div className="content-block-info">
                    <h1 className="content-block-title">{this.state.title}</h1>
                    <p className="content-block-desc">{this.state.content}</p>
                </div>

            </Fragment>
        );
    }
}

export default Pages;
