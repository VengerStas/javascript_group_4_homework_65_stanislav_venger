import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://multipage-d738b.firebaseio.com/'
});

export default instance;
