export const CATEGORIES = {
    'main': 'Main page',
    'about': 'About',
    'clients': 'Clients',
    'cooperation': 'Cooperation',
    'contacts': 'Contacts',
};
