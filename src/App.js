import React, { Component } from 'react';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import './App.css';
import Pages from "./components/Pages/Pages";
import EditPages from "./components/EditPages/EditPages";


class App extends Component {
  render() {
    return (
      <div className="App">
        <BrowserRouter>
          <Switch>
              <Route path="/pages/admin" component={EditPages}/>
              <Route path="/pages/:name" exact component={Pages} />
              <Route path="/" exact component={Pages}/>
          </Switch>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
